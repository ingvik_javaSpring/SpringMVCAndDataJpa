package com.persistenceApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcAndDataJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcAndDataJpaApplication.class, args);
	}

}
