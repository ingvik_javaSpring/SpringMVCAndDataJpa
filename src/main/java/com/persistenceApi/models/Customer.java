package com.persistenceApi.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "Customers")
public class Customer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3931674434160850514L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty // Only works for String data types MUST IMPLEMENT @Valid WHEN INVOQUE
	@Column(length = 100, nullable = false)
	private String name;

	@NotEmpty // Only works for String data types MUST IMPLEMENT @Valid WHEN INVOQUE
	@Column(length = 100, nullable = false, unique = true)
	private String rfc;

	@NotEmpty // Only works for String data types MUST IMPLEMENT @Valid WHEN INVOQUE
	@Email
	private String email;

	@NotNull // Works for Dates, Numbers etc data types MUST IMPLEMENT @Valid WHEN INVOQUE
	@Temporal(TemporalType.DATE)
	@Column(name = "registration_Date")
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date registrationDate;

	@PrePersist
	public void prePersist() {
		log.info("Date before persist :::: {}", getRegistrationDate());
		if (null == getRegistrationDate()) {
			registrationDate = new Date();
		}
	}

}
